package com.grudder.mall.service.order.service.mapper;

import com.grudder.mall.service.order.domain.*;
import com.grudder.mall.service.order.service.dto.OrderItemDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity OrderItem and its DTO OrderItemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrderItemMapper {

    @Mapping(source = "order.id", target = "orderId")
    @Mapping(source = "order.billCode", target = "orderBillCode")
    OrderItemDTO orderItemToOrderItemDTO(OrderItem orderItem);

    List<OrderItemDTO> orderItemsToOrderItemDTOs(List<OrderItem> orderItems);

    @Mapping(source = "orderId", target = "order")
    OrderItem orderItemDTOToOrderItem(OrderItemDTO orderItemDTO);

    List<OrderItem> orderItemDTOsToOrderItems(List<OrderItemDTO> orderItemDTOs);

    default Order orderFromId(Long id) {
        if (id == null) {
            return null;
        }
        Order order = new Order();
        order.setId(id);
        return order;
    }
}
