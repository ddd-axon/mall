package com.grudder.mall.service.order.config;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.grudder.mall.service.order")
public class FeignConfiguration {

}
