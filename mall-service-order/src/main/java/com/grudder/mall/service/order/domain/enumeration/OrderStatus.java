package com.grudder.mall.service.order.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    PENDING_PAY,PAID,DELIVERED,CANCELED,FINISHED
}
