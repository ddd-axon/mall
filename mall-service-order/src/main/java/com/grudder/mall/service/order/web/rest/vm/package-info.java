/**
 * View Models used by Spring MVC REST controllers.
 */
package com.grudder.mall.service.order.web.rest.vm;
