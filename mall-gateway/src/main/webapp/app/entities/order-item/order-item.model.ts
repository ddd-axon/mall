export class OrderItem {
    constructor(
        public id?: number,
        public productId?: string,
        public productName?: string,
        public price?: number,
        public quantity?: number,
        public orderId?: number,
    ) {
    }
}
