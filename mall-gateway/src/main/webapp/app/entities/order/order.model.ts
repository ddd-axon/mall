
const enum OrderStatus {
    'PENDING_PAY',
    'PAID',
    'DELIVERED',
    'CANCELED',
    'FINISHED'

};
export class Order {
    constructor(
        public id?: number,
        public billCode?: string,
        public totalAmount?: number,
        public status?: OrderStatus,
        public shopId?: string,
        public customerId?: string,
        public timeCreated?: any,
        public orderItemId?: number,
    ) {
    }
}
